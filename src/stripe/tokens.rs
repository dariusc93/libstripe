use crate::stripe::{StripePath, UrlPath, Card, BankAccount, CardParam, BankAccountParam, Object};
use crate::{Client, Result};
use serde;
use crate::StripeService;



#[derive(Deserialize, Debug)]
pub struct Tokens {
    pub id: String,
    pub object: Object,
    pub card: Option<Card>,
    pub bank_account: Option<BankAccount>,
    pub client_ip: Option<String>,
    pub created: i64,
    pub livemode: bool,
    #[serde(rename="type")]
    pub token_type: TokenType,
    pub used: bool
}

#[derive(Deserialize, Debug)]
pub enum TokenType {
    #[serde(rename="card")]
    Card,
    #[serde(rename="bank_account")]
    BankAccount
}

#[derive(Default, Serialize, Debug)]
pub struct TokenParam<'a> {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub card: Option<CardParam<'a>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub bank_account: Option<BankAccountParam<'a>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub pii: Option<PIIParam<'a>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub customer: Option<&'a str>
    // #[serde(skip_serializing_if = "Option::is_none")]
    // pub account: Option<...>
}

#[derive(Default, Serialize, Debug)]
pub struct PIIParam<'a> {
    pub personal_id_number: &'a str
}

impl StripeService for Tokens {}
impl<'a> StripeService for TokenParam<'a> {}

impl Tokens {

    pub fn create<B: serde::Serialize + StripeService>(client: &Client, param: B) -> Result<Self> {
        client.post(UrlPath::Tokens, &StripePath::default(), param)
    }

    pub fn retrieve(client: &Client, token: &str) -> Result<Self> {
        client.get(UrlPath::Tokens, &StripePath::default().param(token), Self::object())
    }

}