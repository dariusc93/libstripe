use crate::{Client, Result};
use crate::stripe::{StripePath, UrlPath, Coupon, Object};
use crate::util::Deleted;
use crate::StripeService;



#[derive(Deserialize, Debug)]
pub struct Discount {
    pub object: Object,
    pub coupon: Coupon,
    pub customer: String,
    pub end: i64,
    pub start: i64,
    pub subscription: Option<String>,
}

//TODO: Delete Sub
impl StripeService for Discount {}

impl Discount {

    pub fn delete_customer_discount(client: &Client, cust_id: &str) -> Result<Deleted> {
        client.delete(UrlPath::Customers, &StripePath::default().param(cust_id).param("discount"), Self::object())
    }

    pub fn delete_subscription_discount(client: &Client, sub_id: &str) -> Result<Deleted> {
        client.delete(UrlPath::Subscriptions, &StripePath::default().param(sub_id).param("discount"), Self::object())
    }

}