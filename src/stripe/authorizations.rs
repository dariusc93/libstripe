use crate::stripe::{Object, Currency, StripePath, UrlPath, MerchantCategories, BalanceTransaction};
use std::collections::HashMap;
use crate::{Client, StripeService, Result};
use crate::util::List;
use serde;

#[derive(Deserialize, Debug)]
pub struct Authorizations {
    pub id: String,
    pub object: Object,
    pub approved: bool,
    pub authorization_method: AuthorizationMethod,
    pub authorized_amount: i64,
    pub authorized_currency: Currency,
    pub balance_transactions: Vec<BalanceTransaction>,
    pub card: Option<String>,
    pub cardholder: String,
    pub created: i64,
    pub held_amount: i64,
    pub held_currency: Currency,
    pub is_held_amount_controllable: bool,
    pub livemode: bool,
    pub merchant_data: MerchantData,
    pub metadata: HashMap<String, String>,
    pub pending_authorized_amount: i64,
    pub pending_held_amount: i64,
    pub request_history: Vec<RequestHistory>,
    pub status: AuthorizationStatus,
    pub transactions: Option<String>, //TODO: Transactions
    pub verification_data: VerificationData,

}

#[derive(Serialize, Deserialize, Debug)]
pub enum AuthorizationMethod {
    #[serde(rename = "keyed_in")]
    KeyedIn,
    #[serde(rename = "swipe")]
    Swipe,
    #[serde(rename = "chip")]
    Chip,
    #[serde(rename = "contactless")]
    Contactless,
    #[serde(rename = "online")]
    Online
}

#[derive(Serialize, Deserialize, Debug)]
pub struct MerchantData {
    pub category: MerchantCategories,
    pub city: String,
    pub country: String,
    pub name: String,
    pub network_id: String,
    pub postal_code: String,
    pub state: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct RequestHistory {
    pub approved: bool,
    pub authorized_amount: i64,
    pub authorized_currency: Currency,
    pub created: i64,
    pub held_amount: i64,
    pub held_currency: Currency,
    pub reason: RequestReason,
}

#[derive(Serialize, Deserialize, Debug)]
pub enum RequestReason {
    #[serde(rename = "authorization_controls")]
    AuthorizationControls,
    #[serde(rename = "card_active")]
    CardActive,
    #[serde(rename = "card_inactive")]
    CardInactive,
    #[serde(rename = "insufficient_funds")]
    InsufficientFunds,
    #[serde(rename = "account_compliance_disabled")]
    AccountComplianceDisabled,
    #[serde(rename = "account_inactive")]
    AccountInactive,
    #[serde(rename = "suspected_fraud")]
    SuspectedFraud,
    #[serde(rename = "webhook_approved")]
    WebhookApproved,
    #[serde(rename = "webhook_declined")]
    WebhookDeclined,
    #[serde(rename = "webhook_timeout")]
    WebhookTimeout,
    #[serde(rename = "forced_card_authentication_failed")]
    ForcedCardAuthenticationFailed,
    #[serde(rename = "forced_card_expired")]
    ForcedCardExpired,
    #[serde(rename = "forced_card_lost")]
    ForcedCardLost,
    #[serde(rename = "forced_card_stolen")]
    ForcedCardStolen,
    #[serde(rename = "forced_do_not_honor")]
    ForcedDoNotHonor,
    #[serde(rename = "forced_incorrect_pin")]
    ForcedIncorrectPin,
    #[serde(rename = "forced_insufficient_funds")]
    ForcedInsufficientFunds,
    #[serde(rename = "forced_invalid_account_number")]
    ForcedInvalidAccountNumber,
    #[serde(rename = "forced_invalid_transaction")]
    ForcedInvalidTransaction,
    #[serde(rename = "forced_suspected_fraud")]
    ForcedSuspectedFraud
}

#[derive(Serialize, Deserialize, Debug)]
pub struct VerificationData {
    pub address_line1_check: VerificationCheck,
    pub address_zip_check: VerificationCheck,
    pub cvc_check: VerificationCheck
}

#[derive(Serialize, Deserialize, Debug)]
pub enum VerificationCheck {
    #[serde(rename = "match")]
    Match,
    #[serde(rename = "mismatch")]
    Mismatch,
    #[serde(rename = "not_provided")]
    NotProvided
}

#[derive(Serialize, Deserialize, Debug)]
pub enum AuthorizationStatus {
    #[serde(rename = "pending")]
    Pending,
    #[serde(rename = "reversed")]
    Reversed,
    #[serde(rename = "closed")]
    Closed
}

impl StripeService for Authorizations {}

impl Authorizations {

    pub fn retrieve(client: &Client) -> Result<Self> {
        client.get(UrlPath::Authorizations, &StripePath::default(), Self::object())
    }

    pub fn update<B: serde::Serialize + StripeService>(client: &Client, id: &str, param: B) -> Result<Self> {
        client.post(UrlPath::Authorizations, &StripePath::default().param(id), param)
    }

    pub fn approve<B: serde::Serialize + StripeService>(client: &Client, id: &str, param: B) -> Result<Self> {
        client.post(UrlPath::Authorizations, &StripePath::default().param(id).param("approve"), param)
    }

    pub fn decline<B: serde::Serialize + StripeService>(client: &Client, id: &str) -> Result<Self> {
        client.post(UrlPath::Authorizations, &StripePath::default().param(id).param("decline"), Self::object())
    }

    pub fn list<B: serde::Serialize + StripeService>(client: &Client, param: B) -> Result<List<Self>> {
        client.get(UrlPath::Authorizations, &StripePath::default(), param)
    }

}