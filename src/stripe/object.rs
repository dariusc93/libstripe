
#[derive(Serialize, Deserialize, PartialEq, Eq, Debug)]
pub enum Object {
    #[serde(rename = "issuing.authorization")]
    Authorization,
    #[serde(rename = "balance")]
    Balance,
    #[serde(rename = "balance_transaction")]
    BalanceTransaction,
    #[serde(rename = "charge")]    
    Charge,
    #[serde(rename = "customer")]
    Customer,
    #[serde(rename = "dispute")]
    Dispute,
    #[serde(rename = "event")]
    Event,
    #[serde(rename = "file")]
    File,
    #[serde(rename = "list")]
    List,
    #[serde(rename = "file_link")]
    FileLink,
    #[serde(rename = "payout")]
    Payout,
    #[serde(rename = "product")]
    Product,
    #[serde(rename = "refund")]
    Refund,
    #[serde(rename = "token")]
    Token,
    #[serde(rename = "bank_account")]
    BankAccount,
    #[serde(rename = "card")]
    Card,
    #[serde(rename = "issuing.cardholder")]
    CardHolders,
    #[serde(rename = "source")]
    Source,
    #[serde(rename = "coupon")]
    Coupon,
    #[serde(rename = "discount")]
    Discount,
    #[serde(rename = "invoice")]
    Invoice,
    #[serde(rename = "invoiceitem")]
    InvoiceItem,
    #[serde(rename = "issuing.card")]
    IssuingCard,
    #[serde(rename = "issuing.dispute")]
    IssuingDispute,
    #[serde(rename = "issuing.transaction")]
    Transactions,
    #[serde(rename = "line_item")]
    LineItem,
    #[serde(rename = "plan")]
    Plan,
    #[serde(rename = "payment_intent")]
    PaymentIntent,
    #[serde(rename = "subscription")]
    Subscription,
    #[serde(rename = "subscription_item")]
    SubscriptionItem,
    #[serde(rename = "usagerecord")]
    UsageRecord,
    #[serde(rename = "account")]
    Account,
    #[serde(rename = "login_link")]
    LoginLink,
    #[serde(rename = "fee_refund")]
    FeeRefund,
    #[serde(rename = "platformearning")]
    PlatformEarning,
    #[serde(rename = "country_spec")]
    CountrySpec,
    #[serde(rename = "transfer")]
    Transfer,
    #[serde(rename = "transfer_reversal")]
    TransferReversal,
    #[serde(rename = "topup")]
    Topup,
    #[serde(rename = "review")]
    Review,
    #[serde(rename = "order")]
    Order,
    #[serde(rename = "order_item")]
    OrderItem,
    #[serde(rename = "order_return")]
    OrderReturn,
    #[serde(rename = "sku")]
    SKU,
    #[serde(rename = "scheduled_query_run")]
    ScheduledQueryRun,
    #[serde(skip_serializing)]
    Unknown
}

impl Default for Object {
    fn default() -> Self {
        Object::Unknown
    }
}