use crate::util::{List, RangeQuery};
use crate::{Client, Result};
use crate::stripe::{StripePath, UrlPath, OrderItem, Currency, Object};
use serde;
use crate::StripeService;



#[derive(Debug, Deserialize)]
pub struct Returns {
    pub id: String,
    pub object: Object,
    pub amount: i32,
    pub created: i64,
    pub currency: Currency,
    pub items: Vec<OrderItem>,
    pub livemode: bool,
    pub order: String,
    pub refund: Option<String>
}

#[derive(Default, Serialize, Debug)]
pub struct ReturnListParams<'a> {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub created: Option<RangeQuery>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ending_before: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub limit: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub starting_after: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub order: Option<&'a str>,
}

impl StripeService for Returns {}
impl<'a> StripeService for ReturnListParams<'a> {}

impl Returns {

    pub fn retrieve(client: &Client, id: &str) -> Result<Self> {
        client.get(UrlPath::OrderReturns, &StripePath::default().param(id), Self::object())
    }

    pub fn list<B: serde::Serialize + StripeService>(client: &Client, param: B) -> Result<List<Self>> {
        client.get(UrlPath::OrderReturns, &StripePath::default(), param)
    }

}