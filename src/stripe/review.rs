use crate::stripe::Object;

#[derive(Debug, Deserialize)]
pub struct Review {
    pub id: String,
    pub object: Object,
    pub charge: String,
    pub created: i64,
    pub livemode: bool,
    pub open: bool,
    pub reason: Reason
}

#[derive(Debug, PartialEq, Deserialize)]
pub enum Reason {
    #[serde(rename = "rule")]
    Rule,
    #[serde(rename = "manual")]
    Manual,
    #[serde(rename = "approved")]
    Approved,
    #[serde(rename = "refunded")]
    Refunded,
    #[serde(rename = "refunded_as_fraud")]
    RefundedAsFraud,
    #[serde(rename = "disputed")]
    Disputed
}
