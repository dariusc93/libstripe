use crate::util::List;
use crate::stripe::{StripePath, UrlPath, Currency, Object};
use crate::{Client, Result};
use serde;
use std::collections::HashMap;
use crate::StripeService;


#[derive(Deserialize, Debug)]
pub struct Payout {
    pub id: String,
    pub object: Object,
    pub amount: i64,
    pub arrival_date: i64,
    pub automatic: bool,
    pub balance_transaction: String,
    pub created: i64,
    pub currency: Currency,
    pub description: String,
    pub destination: Option<String>,
    pub failure_balance_transaction: Option<String>,
    pub failure_code: Option<PayoutFailureCode>,
    pub failure_message: Option<String>,
    pub livemode: bool,
    pub metadata: HashMap<String, String>,
    pub method: PayoutMethod,
    pub source_type: PayoutSourceType,
    pub statement_descriptor: Option<String>,
    pub status: PayoutStatus,
    #[serde(rename = "type")]
    pub payout_type: PayoutType
}

#[derive(Deserialize, Debug)]
pub enum PayoutStatus {
    #[serde(rename = "paid")]
    Paid,
    #[serde(rename = "pending")]
    Pending,
    #[serde(rename = "in_transit")]
    InTransit,
    #[serde(rename = "canceled")]
    Canceled,
    #[serde(rename = "failed")]
    Failed
}

#[derive(Serialize, Deserialize, Debug)]
pub enum PayoutSourceType {
    #[serde(rename = "bank_account")]
    BankAccount,
    #[serde(rename = "card")]
    Card,
    #[serde(rename = "alipay_account")]
    Alipay,
}


#[derive(Serialize, Deserialize, Debug)]
pub enum PayoutType {
    #[serde(rename = "bank_account")]
    BankAccount,
    #[serde(rename = "card")]
    Card,
}

#[derive(Deserialize, Debug)]
pub enum PayoutFailureCode {
    #[serde(rename = "account_closed")]
    AccountClosed,
    #[serde(rename = "account_frozen")]
    AccountFrozen,
    #[serde(rename = "bank_account_restricted")]
    BankAccountRestricted,
    #[serde(rename = "bank_ownership_changed")]
    BankOwnershipChanged,
    #[serde(rename = "could_not_process")]
    CouldNotProcess,
    #[serde(rename = "debit_not_authorized")]
    DebitNotAuthorized,
    #[serde(rename = "insufficient_funds")]
    InsufficientFunds,
    #[serde(rename = "invalid_account_number")]
    InvalidAccountNumber,
    #[serde(rename = "invalid_currency")]
    InvalidCurrency,
    #[serde(rename = "no_account")]
    NoAccount,
    #[serde(rename = "unsupported_card")]
    UnsupportedCard
}

#[derive(Serialize, Deserialize, Debug)]
pub enum PayoutMethod {
    #[serde(rename = "standard")]
    STANDARD,
    #[serde(rename = "instant")]
    INSTANT
}


#[derive(Default, Serialize, Debug)]
pub struct PayoutParam<'a> {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub amount: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub currency: Option<Currency>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub destination: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub metadata: Option<HashMap<&'a str, &'a str>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub method: Option<PayoutMethod>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub source_type: Option<PayoutType>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub statement_descriptor: Option<&'a str>,
}

impl StripeService for Payout {}
impl<'a> StripeService for PayoutParam<'a> {}

impl Payout {
    
    pub fn create<B: serde::Serialize + StripeService>(client: &Client, param: B) -> Result<Self> {
        client.post(UrlPath::Payouts, &StripePath::default(), param)
    }

    pub fn retrieve(client: &Client, id: &str) -> Result<Self> {
        client.get(UrlPath::Payouts, &StripePath::default().param(id), Self::object())
    }

    pub fn update<B: serde::Serialize + StripeService>(client: &Client, id: &str, param: B) -> Result<Self> {
        client.post(UrlPath::Payouts, &StripePath::default().param(id), param)
    }

    pub fn list<B: serde::Serialize + StripeService>(client: &Client, param: B) -> Result<List<Self>> {
        client.get(UrlPath::Payouts, &StripePath::default(), param)
    }

    pub fn cancel(client: &Client, id: &str) -> Result<Self> {
        client.post(UrlPath::Payouts, &StripePath::default().param(id).param("cancel"), Self::object())
    }

}