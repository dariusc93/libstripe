use std::collections::HashMap;
use crate::error::ErrorCode;
use crate::stripe::{StripePath, UrlPath, Currency, Refund, PaymentSource, PaymentSourceParam, Object, Address};
use crate::util::{List, RangeQuery};
use crate::{Client, Result};
use serde;
use crate::StripeService;



#[derive(Deserialize, Debug)]
pub struct Charge {
    pub id: String,
    pub object: Object,
    pub amount: i64,
    pub amount_refunded: i64,
    pub application: Option<String>,
    pub application_fee: Option<String>,
    pub balance_transaction: String,
    pub captured: bool,
    pub created: i64, 
    pub currency: Currency,
    pub customer: Option<String>,
    pub description: Option<String>,
    pub destination: Option<String>,
    pub dispute: Option<String>,
    pub failure_code: Option<ErrorCode>,
    pub failure_message: Option<String>,
    pub fraud_details: FraudDetails,
    pub invoice: Option<String>,
    pub livemode: bool,
    pub metadata: HashMap<String, String>,
    pub on_behalf_of: Option<String>,
    pub order: Option<String>,
    pub outcome: Outcome,
    pub paid: bool,
    pub payment_intent: Option<String>,
    pub receipt_email: Option<String>,
    pub receipt_number: Option<String>,
    pub refunded: bool,
    pub refunds: List<Refund>,
    pub review: Option<String>,
    pub shipping: Option<ShippingDetails>,
    pub source: PaymentSource, //TODO: Change into "Source"
    pub source_transfer: Option<String>,
    pub statement_descriptor: Option<String>,
    pub status: ChargeStatus,
    pub transfer: Option<String>,
    pub transfer_group: Option<String>,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub enum ChargeStatus {
    #[serde(rename = "succeeded")]
    Succeeded,
    #[serde(rename = "pending")]
    Pending,
    #[serde(rename = "failed")]
    Failed,
}

#[derive(Deserialize, Debug)]
pub struct Outcome {
    #[serde(rename = "type")]
    pub outcome_type: OutcomeType,
    pub network_status: NetworkStatus,
    pub reason: Option<OutcomeReason>,
    pub risk_level: RiskLevel,
    pub seller_message: Option<String>,
    pub rule: Option<String>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct FraudDetails {
    pub user_report: Option<UserReport>,
    pub stripe_report: Option<StripeReport>,
}

#[derive(Serialize, Deserialize, Debug)]
pub enum UserReport {
    #[serde(rename = "safe")]
    Safe,
    #[serde(rename = "fraudulent")]
    Fraudulent
}

#[derive(Serialize, Deserialize, Debug)]
pub enum StripeReport {
    #[serde(rename = "fraudulent")]
    Fraudulent
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ShippingDetails {
    pub name: String,
    pub address: Address,
    pub carrier: Option<String>,
    pub phone: Option<String>,
    pub tracking_number: Option<String>,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub enum OutcomeType {
    #[serde(rename = "authorized")]
    Authorized,
    #[serde(rename = "manual_review")]
    ManualReview,
    #[serde(rename = "issuer_declined")]
    IssuerDeclined,
    #[serde(rename = "blocked")]
    Blocked,
    #[serde(rename = "invalid")]
    Invalid
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub enum NetworkStatus {
    #[serde(rename = "approved_by_network")]
    ApprovedByNetwork,
    #[serde(rename = "declined_by_network")]
    DeclinedByNetwork,
    #[serde(rename = "not_sent_to_network")]
    NotSentToNetwork,
    #[serde(rename = "reversed_after_approval")]
    ReversedAfterApproval
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub enum RiskLevel {
    #[serde(rename = "normal")]
    Normal,
    #[serde(rename = "elevated")]
    Elevated,
    #[serde(rename = "highest")]
    Highest,
    #[serde(rename = "not_assessed")]
    NotAssessed,
    #[serde(rename = "unknown")]
    Unknown
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub enum OutcomeReason {
    #[serde(rename = "approved_with_id")]
    ApprovedWithID,
    #[serde(rename = "call_issuer")]
    CallIssuer,
    #[serde(rename = "card_not_supported")]
    CardNotSupported,
    #[serde(rename = "card_velocity_exceeded")]
    CardVelocityExceeded,
    #[serde(rename = "currency_not_supported")]
    CurrencyNotSupported,
    #[serde(rename = "do_not_honor")]
    DoNotHonor,
    #[serde(rename = "do_not_try_again")]
    DoNotTryAgain,
    #[serde(rename = "duplicate_transaction")]
    DuplicateTransaction,
    #[serde(rename = "expired_card")]
    ExpiredCard,
    #[serde(rename = "fraudulent")]
    Fraudulent,
    #[serde(rename = "generic_decline")]
    GenericDecline,
    #[serde(rename = "incorrect_number")]
    IncorrectNumber,
    #[serde(rename = "incorrect_cvc")]
    IncorrectCVC,
    #[serde(rename = "incorrect_pin")]
    IncorrectPIN,
    #[serde(rename = "incorrect_zip")]
    IncorrectZip,
    #[serde(rename = "insufficient_funds")]
    InsufficientFunds,
    #[serde(rename = "invalid_account")]
    InvalidAccount,
    #[serde(rename = "invalid_amount")]
    InvalidAmount,
    #[serde(rename = "invalid_cvc")]
    InvalidCVC,
    #[serde(rename = "invalid_expiry_year")]
    InvalidExpiryYear,
    #[serde(rename = "invalid_number")]
    InvalidNumber,
    #[serde(rename = "invalid_pin")]
    InvalidPin,
    #[serde(rename = "issuer_not_available")]
    IssuerNotAvailable,
    #[serde(rename = "lost_card")]
    LostCard,
    #[serde(rename = "new_account_information_available")]
    NewAccountInformationAvailable,
    #[serde(rename = "no_action_taken")]
    NoActionTaken,
    #[serde(rename = "not_permitted")]
    NotPermitted,
    #[serde(rename = "pickup_card")]
    PickupCard,
    #[serde(rename = "pin_try_exceeded")]
    PinTryExceeded,
    #[serde(rename = "processing_error")]
    ProcessingError,
    #[serde(rename = "reenter_transaction")]
    ReenterTransaction,
    #[serde(rename = "restricted_card")]
    RestrictedCard,
    #[serde(rename = "revocation_of_all_authorization")]
    RevocationOfAllAuthorization,
    #[serde(rename = "revocation_of_authorization")]
    RevocationOfAuthorization,
    #[serde(rename = "security_violation")]
    SecurityViolation,
    #[serde(rename = "service_not_allowed")]
    ServiceNotAllowed,
    #[serde(rename = "stolen_card")]
    StolenCard,
    #[serde(rename = "stop_payment_order")]
    StopPaymentOrder,
    #[serde(rename = "testmode_declined")]
    TestmodeDeclined,
    #[serde(rename = "transaction_not_allowed")]
    TransactionNotAllowed,
    #[serde(rename = "try_again_later")]
    TryAgainLater,
    #[serde(rename = "withrawal_count_limit_exceeded")]
    WithrawalCountLimitExceeded
}

#[derive(Default, Serialize, Debug)]
pub struct ChargeParams<'a> {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub amount: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub currency: Option<Currency>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub application_fee: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub capture: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub destination: Option<&'a str>, //TODO
    #[serde(skip_serializing_if = "Option::is_none")]
    pub transfer_group: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub on_behalf_of: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub metadata: Option<HashMap<String, String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub receipt_email: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub shipping: Option<ShippingDetails>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub customer: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub source: Option<PaymentSourceParam<'a>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub statement_descriptor: Option<&'a str>,
    //Used for updates
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fraud_details: Option<FraudDetails>
}

#[derive(Default, Serialize, Debug)]
pub struct ChargeListParam<'a> {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub customer: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub created: Option<RangeQuery>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ending_before: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub limit: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub source: Option<ChargeSourceListParam>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub starting_after: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub transfer_group: Option<&'a str>,
}

#[derive(Serialize, Debug)]
pub struct ChargeSourceListParam {
    pub object: ChargeSourceObject,
}

#[derive(Serialize, Debug)]
pub enum ChargeSourceObject {
    #[serde(rename = "all")]
    All,
    #[serde(rename = "alipay_account")]
    AlipayAccount,
    #[serde(rename = "bank_account")]
    BankAccount,
    #[serde(rename = "card")]
    Card
}

impl StripeService for Charge {}
impl<'a> StripeService for ChargeParams<'a> {}
impl StripeService for ChargeSourceListParam {}

impl Charge {
    
    pub fn create<B: serde::Serialize + StripeService>(client: &Client, param: B) -> Result<Self> {
        client.post(UrlPath::Charges, &StripePath::default(), param)
    }

    pub fn retrieve(client: &Client, id: &str) -> Result<Self> {
        client.get(UrlPath::Charges, &StripePath::default().param(id), Self::object())
    }

    pub fn update<B: serde::Serialize + StripeService>(client: &Client, id: &str, param: B) -> Result<Self> {
        client.post(UrlPath::Charges, &StripePath::default().param(id), param)
    }

    pub fn capture<B: serde::Serialize + StripeService>(client: &Client, id: &str, param: B) -> Result<Self> {
        client.post(UrlPath::Charges, &StripePath::default().param(id), param)
    }

    pub fn list<B: serde::Serialize + StripeService>(client: &Client, param: B) -> Result<List<Self>> {
        client.get(UrlPath::Charges, &StripePath::default(), param)
    }    
    
}