use crate::{Client, Result};
use crate::stripe::{StripePath, UrlPath, File, Object};
use crate::util::List;
use serde::Serialize;
use serde_json::Value;
use crate::StripeService;



#[derive(Debug, Deserialize)]
pub struct ScheduledQueryRun {
    pub id: String,
    pub object: Object,
    pub created: i64,
    pub data_load_time: i64,
    pub error: Option<Value>, //TODO
    pub file: Option<File>,
    pub livemode: bool,
    pub result_available_until: i64,
    pub sql: String,
    pub status: SigmaStatus,
    pub title: String,
}

#[derive(Debug, Deserialize)]
pub enum SigmaStatus {
    #[serde(rename = "completed")]
    Completed,
    #[serde(rename = "canceled")]
    Canceled,
    #[serde(rename = "failed")]
    Failed,
    #[serde(rename = "timed_out")]
    TimedOut
}

#[derive(Debug, Serialize)]
pub struct ScheduleQueryRunListParam<'a> {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ending_before: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub limit: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub starting_after: Option<&'a str>,
}

impl StripeService for ScheduledQueryRun {}
impl<'a> StripeService for ScheduleQueryRunListParam<'a> {}

impl ScheduledQueryRun {

    pub fn retrieve(client: &Client, id: &str) -> Result<Self> {
        client.get(UrlPath::Sigma, &StripePath::default().param("scheduled_query_run").param(id), Self::object())
    }

    pub fn list<B: Serialize + StripeService>(client: &Client, param: B) -> Result<List<Self>> {
        client.get(UrlPath::Sigma, &StripePath::default().param("scheduled_query_run"), param)
    }

}