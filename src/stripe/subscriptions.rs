use crate::stripe::{StripePath, UrlPath, Plans, PaymentSourceParam, Object};
use crate::util::List;
use crate::{Client, Result};
use serde;
use std::collections::HashMap;
use crate::StripeService;



#[derive(Deserialize, Debug)]
pub struct Subscription {
    pub id: String,
    pub object: Object,
    pub application_fee_percent: Option<i32>,
    pub cancel_at_period_end: bool,
    pub canceled_at: Option<i64>,
    pub created: i64,
    pub current_period_end: i64,
    pub current_period_start: i64,
    pub customer: String,
    pub discount: Option<String>,
    pub ended_at: Option<i64>,
    pub items: List<SubscriptionItems>,
    pub livemode: bool,
    pub metadata: HashMap<String, String>,
    pub plan: Plans,
    pub quantity: i64,
    pub start: i64,
    pub status: SubscriptionStatus,
    pub tax_percent: Option<f64>,
    pub trial_end: Option<i64>,
    pub trial_start: Option<i64>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct SubscriptionItems {
    pub id: String,
    pub object: String,
    pub created: i64,
    pub metadata: HashMap<String, String>,
    pub plan: Plans,
    pub quantity: i64
}


#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub enum SubscriptionStatus {
    #[serde(rename="trialing")]
    Trialing,
    #[serde(rename="active")]
    Active,
    #[serde(rename="past_due")]
    PastDue,
    #[serde(rename="canceled")]
    Canceled,
    #[serde(rename="unpaid")]
    Unpaid
}

#[derive(Default, Serialize, Debug)]
pub struct SubscriptionItemParam<'a> {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub plan: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub subscription: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub metadata: Option<HashMap<String, String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub prorate: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub prorate_date: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub quantity: Option<i64>
}

impl StripeService for SubscriptionItems {}
impl<'a> StripeService for SubscriptionItemParam<'a> {}

impl SubscriptionItems  {

    pub fn create<B: serde::Serialize + StripeService>(client: &Client, param: B) -> Result<Self> {
        client.post(UrlPath::SubscriptionItems, &StripePath::default(), param)
    }

    pub fn retrieve(client: &Client, id: &str) -> Result<Self> {
        client.get(UrlPath::SubscriptionItems, &StripePath::default().param(id), Self::object())
    }

    pub fn update<B: serde::Serialize + StripeService>(client: &Client, id: &str, param: B) -> Result<Self> {
        client.post(UrlPath::SubscriptionItems, &StripePath::default().param(id), param)
    }

    pub fn delete<B: serde::Serialize + StripeService>(client: &Client, id: &str, param: B) -> Result<Self> {
        client.delete(UrlPath::SubscriptionItems, &StripePath::default().param(id), param)
    }

    pub fn list<B: serde::Serialize + StripeService>(client: &Client, param: B) -> Result<List<Self>> {
        client.get(UrlPath::SubscriptionItems, &StripePath::default(), param)
    }

}

#[derive(Default, Serialize, Debug)]
pub struct SubscriptionParam<'a> {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub customer: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub application_fee_percent: Option<f32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub coupon: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub items: Option<Vec<ItemParam<'a>>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub metadata: Option<HashMap<String, String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub source: Option<PaymentSourceParam<'a>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub tax_percent: Option<f32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub trial_end: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub trail_period_days: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub prorate: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub proration_date: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub at_period_end: Option<bool>,
}

#[derive(Default, Serialize, Debug)]
pub struct ItemParam<'a> {
    pub plan: &'a str,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub quantity: Option<i32>
}

impl StripeService for Subscription {}
impl<'a> StripeService for SubscriptionParam<'a> {}
impl<'a> StripeService for ItemParam<'a> {}


impl Subscription {

    pub fn create<B: serde::Serialize + StripeService>(client: &Client, param: B) -> Result<Self> {
        client.post(UrlPath::Subscriptions, &StripePath::default(), param)
    }

    pub fn retrieve(client: &Client, id: &str) -> Result<Self> {
        client.get(UrlPath::Subscriptions, &StripePath::default().param(id), Self::object())
    }

    pub fn update<B: serde::Serialize + StripeService>(client: &Client, id: &str, param: B) -> Result<Self> {
        client.post(UrlPath::Subscriptions, &StripePath::default().param(id), param)
    }

    pub fn cancel<B: serde::Serialize + StripeService>(client: &Client, id: &str, param: B) -> Result<Self> {
        client.delete(UrlPath::Subscriptions, &StripePath::default().param(id), param)
    }

    pub fn list<B: serde::Serialize + StripeService>(client: &Client, param: B) -> Result<List<Self>> {
        client.get(UrlPath::Subscriptions, &StripePath::default(), param)
    }
}