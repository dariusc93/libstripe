use crate::stripe::{StripePath, UrlPath, Currency, ProductsParam, Object};
use crate::util::Deleted;
use crate::{Client, Result};
use serde;
use std::collections::HashMap;
use crate::StripeService;



#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct Plans {
    pub id: String,
    pub object: Object,
    pub amount: i64,
    pub created: i64,
    pub currency: Currency,
    pub interval: Interval,
    pub billing_scheme: Option<BillingScheme>,
    pub interval_count: i64,
    pub livemode: bool,
    pub metadata: HashMap<String, String>,
    pub nickname: Option<String>,
    pub product: Option<String>,
    pub tiers: Option<Tiers>,
    pub tiers_mode: Option<TiersMode>,
    pub usage_type: Option<UsageType>,
    pub transform_usage: Option<String>,
    pub trial_period_days: Option<i64>,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct Tiers {
    pub amount: i64,
    pub up_to: Option<String>
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub enum BillingScheme {
    #[serde(rename = "per_unit")]
    PerUnit,
    #[serde(rename = "tiered")]
    Tiered,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub enum TiersMode {
    #[serde(rename = "graduated")]
    Graduated,
    #[serde(rename = "volume")]
    Volume
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct TransformUsage {
    pub divide_by: f64,
    pub round: Round,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub enum Round {
    #[serde(rename = "up")]
    UP,
    #[serde(rename = "down")]
    DOWN
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub enum UsageType {
    #[serde(rename = "metered")]
    Metered,
    #[serde(rename = "licensed")]
    Licensed
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub enum Interval {
    #[serde(rename = "day")]
    Day,
    #[serde(rename = "week")]
    Week,
    #[serde(rename = "month")]
    Month,
    #[serde(rename = "year")]
    Year
}

#[derive(Default, Debug, Serialize)]
pub struct PlansParam<'a> {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub amount: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub currency: Option<Currency>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub interval: Option<Interval>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub nickname: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub interval_count: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub metadata: Option<HashMap<&'a str, &'a str>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub product: Option<ProductsParam<'a>>,
}

impl StripeService for Plans {}
impl<'a> StripeService for PlansParam<'a> {}

impl Plans {

    pub fn create<B: serde::Serialize + StripeService>(client: &Client, param: B) -> Result<Self> {
        client.post(UrlPath::Plans, &StripePath::default(), param)
    }

    pub fn retrieve(client: &Client, plan: &str) -> Result<Self> {
        client.get(UrlPath::Plans, &StripePath::default().param(plan), Self::object())
    }

    pub fn update<B: serde::Serialize + StripeService>(client: &Client, plan: &str, param: B) -> Result<Self> {
        client.post(UrlPath::Plans, &StripePath::default().param(plan), param)
    }

    pub fn delete(client: &Client, plan: &str) -> Result<Deleted> {
        client.delete(UrlPath::Plans, &StripePath::default().param(plan), Self::object())
    }
}