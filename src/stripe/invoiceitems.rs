use crate::stripe::{StripePath, UrlPath, Currency, Plans, Object};
use crate::util::{List, Period, Deleted, RangeQuery};
use crate::{Client, Result};
use serde;
use std::collections::HashMap;
use crate::StripeService;



#[derive(Deserialize, Debug)]
pub struct InvoiceItems {
    pub id: String,
    pub object: Object,
    pub amount: i64,
    pub currency: Currency,
    pub customer: String,
    pub date: i64,
    pub description: Option<String>,
    pub discountable: bool,
    pub invoice: Option<String>,
    pub livemode: bool,
    pub metadata: HashMap<String, String>,
    pub period: Period,
    pub plan: Option<Plans>,
    pub proration: bool,
    pub quantity: Option<i64>,
    pub subscription: Option<String>,
    pub subscription_item: Option<String>
}

#[derive(Default, Serialize, Debug)]
pub struct InvoiceItemsParam<'a> {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub amount: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub currency: Option<Currency>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub customer: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub discountable: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub invoice: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub metadata: Option<HashMap<&'a str, &'a str>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub subscription: Option<&'a str>
}


#[derive(Default, Serialize, Debug)]
pub struct InvoiceItemsListParams<'a> {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub created: Option<RangeQuery>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub customer: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ending_before: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub invoice: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub limit: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub starting_after: Option<&'a str>,
}

impl StripeService for InvoiceItems {}
impl<'a> StripeService for InvoiceItemsParam<'a> {}
impl<'a> StripeService for InvoiceItemsListParams<'a> {}

impl InvoiceItems {
    
    pub fn create<B: serde::Serialize + StripeService>(client: &Client, param: B) -> Result<Self> {
        client.post(UrlPath::InvoiceItems, &StripePath::default(), param)
    }

    pub fn retrieve(client: &Client, id: &str) -> Result<Self> {
        client.get(UrlPath::InvoiceItems, &StripePath::default().param(id), Self::object())
    }

    pub fn update<B: serde::Serialize + StripeService>(client: &Client, id: &str, param: B) -> Result<Self> {
        client.post(UrlPath::InvoiceItems, &StripePath::default().param(id), param)
    }

    pub fn delete(client: &Client, id: &str) -> Result<Deleted> {
        client.delete(UrlPath::InvoiceItems, &StripePath::default().param(id), Self::object())
    }

    pub fn list<B: serde::Serialize + StripeService>(client: &Client, param: B) -> Result<List<Self>> {
        client.get(UrlPath::InvoiceItems, &StripePath::default(), param)
    }

}