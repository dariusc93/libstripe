use serde;
use crate::stripe::{StripePath, UrlPath, Currency, Object};
use crate::util::{List, RangeQuery};
use crate::{Client, Result, StripeService};



#[derive(Deserialize, Debug)]
pub struct Balance {
    pub object: Object,
    pub available: Vec<BalanceSource>,
    pub connect_reserved: Vec<BalanceSource>,
    pub pending: Vec<BalanceSource>,
    pub livemode: bool,
}

#[derive(Deserialize, Debug)]
pub struct BalanceSource {
    pub currency: Currency,
    pub amount: i64,
    pub source_types: Option<BalanceSourceType>
}

#[derive(Deserialize, Debug)]
pub struct BalanceSourceType {
    pub card: Option<i64>,
    pub bank_account: Option<i64>,
    pub bitcoin_receiver: Option<i64>
}

#[derive(Deserialize, Debug)]
pub struct BalanceTransaction {
    pub id: String,
    pub object: Object,
    pub amount: i64,
    pub available_on: i64,
    pub created: i64,
    pub currency: Currency,
    pub description: Option<String>,
    pub exchange_rate: Option<f64>,
    pub fee: i64,
    pub fee_details: Vec<FeeDetails>,
    pub net: i64,
    pub source: String,
    pub status: BalanceStatus,
    #[serde(rename="type")]
    pub balance_type: BalanceType
}

#[derive(Deserialize, Debug)]
pub enum BalanceStatus {
    #[serde(rename="available")]
    Available,
    #[serde(rename="pending")]
    Pending
}

#[derive(Serialize, Deserialize, Debug)]
pub enum BalanceType {
    #[serde(rename="adjustment")]
    Adjustment,
    #[serde(rename="application_fee")]
    ApplicationFee,
    #[serde(rename="application_fee_refund")]
    ApplicationFeeRefund,
    #[serde(rename="charge")]
    Charge,
    #[serde(rename="payment")]
    Payment,
    #[serde(rename="payment_failure_refund")]
    PaymentFailureRefund,
    #[serde(rename="payment_refund")]
    PaymentRefund,
    #[serde(rename="refund")]
    Refund,
    #[serde(rename="transfer")]
    Transfer,
    #[serde(rename="transfer_refund")]
    TransferRefund,
    #[serde(rename="payout")]
    Payout,
    #[serde(rename="payout_cancel")]
    PayoutCancel,
    #[serde(rename="payout_failure")]
    PayoutFailure,
    #[serde(rename="validation")]
    Validation,
    #[serde(rename="stripe_fee")]
    StripeFee,
    #[serde(rename = "network_cost")]
    NetworkCost,
}

#[derive(Deserialize, Debug)]
pub struct FeeDetails {
    pub amount: i64,
    pub application: Option<String>,
    pub currency: Currency,
    pub description: String,
    #[serde(rename="type")]
    pub fee_type: FeeType
}

#[derive(Deserialize, Debug)]
pub enum FeeType {
    #[serde(rename="application_fee")]
    ApplicationFee,
    #[serde(rename="stripe_fee")]
    StripeFee,
    #[serde(rename="tax")]
    Tax
}

#[derive(Default, Serialize, Debug)]
pub struct BalanceListParams<'a> {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub available_on: Option<RangeQuery>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub created: Option<RangeQuery>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ending_before: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub limit: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub starting_after: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub source: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub payout: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub currency: Option<Currency>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename="type")]
    pub balance_type: Option<BalanceType>,
}

impl StripeService for Balance {}
impl StripeService for BalanceTransaction {}
impl<'a> StripeService for BalanceListParams<'a> {}

impl Balance {

    pub fn retrieve(client: &Client) -> Result<Self> {
        client.get(UrlPath::Balance, &StripePath::default(), Balance::object())
    }

}

impl BalanceTransaction {

    pub fn retrieve_transaction(client: &Client, txn: String) -> Result<Self> {
        client.get(UrlPath::Balance, &StripePath::default().param("history").param(txn), Balance::object())
    }

    pub fn list<B: serde::Serialize + StripeService>(client: &Client, param: B) -> Result<List<Self>> {
        client.get(UrlPath::Balance, &StripePath::default().param("history"), param)
    }

}