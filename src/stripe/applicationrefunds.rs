use crate::stripe::{StripePath, UrlPath, BalanceTransaction, Currency, Object};
use std::collections::HashMap;
use crate::util::List;
use crate::{Client, Result, StripeService};
use serde;



#[derive(Deserialize, Debug)]
pub struct ApplicationFeeRefunds {
    pub id: String,
    pub object: Object,
    pub amount: i64,
    pub balance_transaction: Option<BalanceTransaction>,
    pub currency: Currency,
    pub fee: String,
    pub metadata: HashMap<String, String>
}

impl StripeService for ApplicationFeeRefunds {}

impl ApplicationFeeRefunds {

    pub fn create<B: serde::Serialize + StripeService>(client: &Client, id: &str, param: B) -> Result<Self> {
        client.post(UrlPath::ApplicationFees, &StripePath::default().param(id).param("refunds"), param)
    }

    pub fn retrieve(client: &Client, id: &str, fee: &str) -> Result<Self> {
        client.get(UrlPath::ApplicationFees, &StripePath::default().param(id).param("refunds").param(fee), Self::object())
    }

    pub fn update<B: serde::Serialize + StripeService>(client: &Client, id: &str, refund: &str, param: B) -> Result<Self> {
        client.post(UrlPath::ApplicationFees, &StripePath::default().param(id).param("refunds").param(refund), param)
    }

    pub fn list<B: serde::Serialize + StripeService>(client: &Client, id: &str, param: B) -> Result<List<Self>> {
        client.get(UrlPath::ApplicationFees, &StripePath::default().param(id), param)
    }

}