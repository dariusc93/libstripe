use crate::stripe::{StripePath, AccountHolderType, UrlPath, Address, ExternalAccounts, Currency, MinimumVerification, Object};
use crate::util::{List, Deleted};
use std::collections::HashMap;
use serde::Serialize;
use crate::{StripeService, Client, Result};



#[derive(Deserialize, Debug)]
pub struct Account {
    pub id: String,
    pub object: Object,
    pub business_logo: Option<String>,
    pub business_name: Option<String>,
    pub business_url: Option<String>,
    pub charges_enabled: bool,
    pub country: String,
    pub debit_negative_balances: Option<bool>,
    pub decline_charge_on: Option<AccountDeclineChargeOn>,
    pub default_currency: Currency,
    pub details_submitted: bool,
    pub display_name: Option<String>,
    pub email: Option<String>,
    pub external_accounts: Option<List<ExternalAccounts>>,
    pub legal_entity: Option<LegalEntity>,
    pub metadata: Option<HashMap<String, String>>,
    pub payout_schedule: Option<PayoutSchedule>,
    pub payout_statement_descriptor: Option<String>,
    pub payouts_enabled: bool,
    pub product_description: Option<String>,
    pub statement_descriptor: Option<String>,
    pub support_address: Option<Address>,
    pub support_email: Option<String>,
    pub support_phone: Option<String>,
    pub support_url: Option<String>,
    pub timezone: String,
    pub tos_acceptance: Option<AccountTosAcceptance>,
    #[serde(rename="type")]
    pub account_type: AccountType,
    pub verification: Option<AccountVerification>

}

#[derive(Serialize, Deserialize, Debug)]
pub enum AccountType {
    #[serde(rename="standard")]
    STANDARD,
    #[serde(rename="express")]
    EXPRESS,
    #[serde(rename="custom")]
    CUSTOM
}

#[derive(Default, Serialize, Deserialize, Debug)]
pub struct LegalEntity {
    pub additional_owners: Vec<AdditionalOwners>,
    pub address: Option<Address>,
    pub address_kana: Option<Address>,
    pub address_kanji: Option<Address>,
    pub business_name: Option<String>,
    pub business_name_kana: Option<String>,
    pub business_name_kanji: Option<String>,
    pub business_tax_id: Option<String>,
    pub business_vat_id: Option<String>,
    pub business_tax_id_provided: bool,
    pub dob: LegalEntityDoB,
    pub first_name: Option<String>,
    pub first_name_kana: Option<String>,
    pub first_name_kanji: Option<String>,
    pub gender: Option<String>,
    pub last_name: Option<String>,
    pub last_name_kana: Option<String>,
    pub last_name_kanji: Option<String>,
    pub maiden_name: Option<String>,
    pub personal_address: Option<Address>,
    pub personal_address_kana: Option<Address>,
    pub personal_address_kanji: Option<Address>,
    pub personal_id_number_provided: bool,
    pub ssn_last_4_provided: bool,
    #[serde(rename = "type")]
    pub entity_type: Option<AccountHolderType>,
    pub verification: LegalEntityVerification
}

#[derive(Default, Serialize, Deserialize, Debug)]
pub struct AdditionalOwners {
    pub address: Address,
    pub dob: LegalEntityDoB,
    pub first_name: Option<String>,
    pub last_name: Option<String>,
    pub maiden_name: Option<String>,
    pub verification: LegalEntityVerification
}

#[derive(Default, Serialize, Deserialize, Debug)]
pub struct LegalEntityVerification {
    pub details: Option<String>,
    pub details_code: Option<LegalEntityDetailsCode>,
    pub document: Option<String>,
    pub status: Option<LegalEntityVerificationStatus>
}

#[derive(Serialize, Deserialize, Debug)]
pub enum LegalEntityDetailsCode {
    #[serde(rename = "scan_corrupt")]
    ScanCorrupt,
    #[serde(rename = "scan_not_readable")]
    ScanNotReadable,
    #[serde(rename = "scan_failed_greyscale")]
    ScanFailedGreyscale,
    #[serde(rename = "scan_not_uploaded")]
    ScanNotUploaded,
    #[serde(rename = "scan_id_type_not_supported")]
    ScanIdTypeNotSupported,
    #[serde(rename = "scan_id_country_not_supported")]
    ScanIdCountryNotSupported,
    #[serde(rename = "scan_name_mismatch")]
    ScanNameMismatch,
    #[serde(rename = "scan_failed_other")]
    ScanFailedOther,
    #[serde(rename = "failed_keyed_identity")]
    FailedKeyedIdentity,
    #[serde(rename = "failed_other")]
    FailedOther
}

#[derive(Serialize, Deserialize, Debug)]
pub enum LegalEntityVerificationStatus {
    #[serde(rename = "verified")]
    Verified,
    #[serde(rename = "pending")]
    Pending,
    #[serde(rename = "unverified")]
    Unverified
}

#[derive(Default, Serialize, Deserialize, Debug)]
pub struct LegalEntityDoB {
    pub day: Option<i16>,
    pub month: Option<i16>,
    pub year: Option<i16>,
}

#[derive(Deserialize, Debug)]
pub struct AccountTosAcceptance {
    pub date: Option<i64>,
    pub iovation_blackbox: Option<String>,
    pub ip: Option<String>,
    pub user_agent: Option<String>,
}

#[derive(Deserialize, Debug)]
pub struct AccountVerification {
    pub disabled_reason: AccountDisabledReason,
    pub due_by: Option<i64>,
    pub fields_needed: Vec<MinimumVerification>,
}

#[derive(Deserialize, Debug)]
pub enum AccountDisabledReason {
    #[serde(rename = "rejected.fraud")]
    RejectedFraud,
    #[serde(rename = "rejected.terms_of_service")]
    RejectedTermsOfService,
    #[serde(rename = "rejected.listed")]
    RejectedListed,
    #[serde(rename = "rejected.other")]
    RejectedOther,
    #[serde(rename = "fields_needed")]
    FieldsNeeded,
    #[serde(rename = "listed")]
    Listed,
    #[serde(rename = "under_review")]
    UnderReview,
    #[serde(rename = "other")]
    Other
}

#[derive(Deserialize, Debug)]
pub struct PayoutSchedule {
    pub delay_days: i64,
    pub interval: String
}

#[derive(Deserialize, Debug)]
pub struct AccountDeclineChargeOn {
    pub avs_failure: bool,
    pub cvc_failure: bool,
}

#[derive(Serialize, Debug)]
pub enum AccountRejectReason {
    #[serde(rename = "fraud")]
    Fraud,
}

#[derive(Deserialize, Debug)]
pub struct LoginLink {
    pub object: Object,
    pub created: i64,
    pub url: String
}

impl StripeService for Account {}

//For now, this is for external/connect accounts
impl Account {

    pub fn create<B: Serialize + StripeService>(client: &Client, param: B) -> Result<Self> {
        client.post(UrlPath::Accounts, &StripePath::default(), param)
    }

    pub fn retrieve(client: &Client, id: &str) -> Result<Self> {
        client.get(UrlPath::Accounts, &StripePath::default().param(id), Self::object())
    }

    pub fn update<B: Serialize + StripeService>(client: &Client, id: &str, param: B) -> Result<Self> {
        client.post(UrlPath::Accounts, &StripePath::default().param(id), param)
    }

    pub fn delete(client: &Client, id: &str) -> Result<Deleted> {
        client.delete(UrlPath::Accounts, &StripePath::default().param(id), Self::object())
    }

    pub fn reject<B: Serialize + StripeService>(client: &Client, id: &str, param: B) -> Result<Self> {
        client.post(UrlPath::Accounts, &StripePath::default().param(id).param("reject"), param)
    }

    pub fn list<B: Serialize + StripeService>(client: &Client, param: B) -> Result<List<Self>> {
        client.get(UrlPath::Accounts, &StripePath::default(), param)
    }

}

impl StripeService for LoginLink {}

impl LoginLink {

    pub fn create(client: &Client, account: &str) -> Result<Self> {
        client.post(UrlPath::Accounts, &StripePath::default().param(account).param("login_links"), Self::object())
    }

}