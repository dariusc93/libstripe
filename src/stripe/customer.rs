use crate::stripe::{StripePath, UrlPath, Address, Subscription, Currency, PaymentSource, PaymentSourceParam, Object};
use crate::{Client, Result};
use crate::util::{List, Deleted, RangeQuery};
use serde;
use std::collections::HashMap;
use crate::StripeService;



#[derive(Debug, Serialize, Deserialize)]
pub struct CustomerShipping {
    pub address: Address,
    pub name: String,
    pub phone: String,
}

#[derive(Deserialize, Debug)]
pub struct Customer {
    pub id: String,
    pub object: Object,
    pub account_balance: i64,
    pub business_vat_id: Option<String>,
    pub created: i64,
    pub currency: Option<Currency>,
    pub default_source: Option<String>,
    pub delinquent: bool,
    pub description: Option<String>,
    pub discount: Option<String>,
    pub email: Option<String>,
    pub invoice_prefix: Option<String>,
    pub livemode: bool,
    pub metadata: HashMap<String, String>,
    pub shipping: Option<CustomerShipping>,
    pub sources: Option<List<PaymentSource>>,
    pub subscription: Option<List<Subscription>>,
    pub tax_info: Option<TaxInfo>,
    pub tax_info_verification: Option<TaxInfoVerification>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct TaxInfo {
    pub tax_id: String,
    #[serde(rename = "type")]
    pub tax_type: String,
}

#[derive(Debug, Deserialize)]
pub struct TaxInfoVerification {
    pub verified_name: String,
    pub status: TaxStatus
}

#[derive(Debug, Deserialize)]
pub enum TaxStatus {
    Unverified,
    Pending,
    Verified
}

#[derive(Default, Debug, Serialize)]
pub struct CustomerParam<'a> {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub account_balance: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub business_vat_id: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub coupon: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub default_source: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub email: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub invoice_prefix: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub metadata: Option<HashMap<String, String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub shipping: Option<CustomerShipping>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub source: Option<PaymentSourceParam<'a>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub tax_info: Option<TaxInfo>,
}

#[derive(Default, Serialize, Debug)]
pub struct CustomerListParams<'a> {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub created: Option<RangeQuery>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub email: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ending_before: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub limit: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub starting_after: Option<&'a str>,
}

impl StripeService for Customer {}
impl<'a> StripeService for CustomerParam<'a> {}
impl<'a> StripeService for CustomerListParams<'a> {}

impl Customer {

    pub fn create<B: serde::Serialize + StripeService>(client: &Client, param: B) -> Result<Self> {
        client.post(UrlPath::Customers, &StripePath::default(), param)
    }

    pub fn retrieve(client: &Client, customer_id: &str) -> Result<Self> {
        client.get(UrlPath::Customers, &StripePath::default().param(customer_id), Self::object())
    }

    pub fn update<B: serde::Serialize + StripeService>(client: &Client, customer_id: &str, param: B) -> Result<Self> {
        client.post(UrlPath::Customers, &StripePath::default().param(customer_id), param)
    }

    pub fn delete(client: &Client, customer_id: &str) -> Result<Deleted> {
        client.delete(UrlPath::Customers, &StripePath::default().param(customer_id), Self::object())
    }

    pub fn list<B: serde::Serialize + StripeService>(client: &Client, param: B) -> Result<List<Self>> {
        client.get(UrlPath::Customers, &StripePath::default(), param)
    }
}