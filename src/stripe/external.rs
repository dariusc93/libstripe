use crate::stripe::{StripePath, UrlPath, Card, BankAccount};
use crate::util::{List, Deleted};
use serde;
use crate::{Client, Result};
use crate::StripeService;



#[derive(Deserialize, Debug)]
pub enum ExternalAccounts {
    #[serde(rename="card")]
    Card(Card),
    #[serde(rename="bank_account")]
    BankAccount(BankAccount)
}

impl StripeService for ExternalAccounts {}

impl ExternalAccounts {

    pub fn create<B: serde::Serialize + StripeService>(client: &Client, acct: &str, param: B) -> Result<Self> {
        client.post(UrlPath::Accounts, &StripePath::default().param(acct).param("external_accounts"), param)
    }

    pub fn retrieve(client: &Client, acct: &str, id: &str) -> Result<Self> {
        client.get(UrlPath::Accounts, &StripePath::default().param(acct).param("external_accounts").param(id), Self::object())
    }

    pub fn update<B: serde::Serialize + StripeService>(client: &Client, acct: &str, id: &str, param: B) -> Result<Self> {
        client.post(UrlPath::Accounts, &StripePath::default().param(acct).param("external_accounts").param(id), param)
    }

    pub fn delete(client: &Client, acct: &str, id: &str) -> Result<Deleted> {
        client.delete(UrlPath::Accounts, &StripePath::default().param(acct).param("external_accounts").param(id), Self::object())
    }

    pub fn list<B: serde::Serialize + StripeService>(client: &Client, acct: &str, id: &str, param: B) -> Result<List<Self>> {
        client.get(UrlPath::Accounts, &StripePath::default().param(acct).param("external_accounts").param(id), param)
    }
}