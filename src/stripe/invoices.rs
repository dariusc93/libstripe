use crate::stripe::{StripePath, UrlPath, Plans, Currency, Discount, SubscriptionItems, Object};
use crate::util::{List, Period, RangeQuery};
use crate::{Client, Result};
use serde;
use std::collections::HashMap;
use crate::StripeService;



#[derive(Deserialize, Debug)]
pub struct Invoice {
    pub id: String,
    pub object: Object,
    pub amount_due: i64,
    pub amount_paid: i64,
    pub amount_remaining: i64,
    pub application_fee: Option<i64>,
    pub attempt_count: i64,
    pub attempted: bool,
    pub auto_advance: bool,
    pub billing: InvoiceBilling,
    pub billing_reason: InvoiceBillingReason,
    pub charge: Option<String>,
    pub currency: Currency,
    pub customer: String,
    pub date: i64,
    pub default_source: Option<String>,
    pub description: Option<String>,
    pub discount: Option<Discount>,
    pub due_date: Option<i64>,
    pub ending_balance: Option<i64>,
    pub finalized_at: Option<i64>,
    pub hosted_invoice_url: Option<String>,
    pub invoice_pdf: Option<String>,
    pub lines: List<InvoiceLine>,
    pub livemode: bool,
    pub metadata: HashMap<String, String>,
    pub next_payment_attempt: Option<i64>,
    pub number: Option<String>,
    pub paid: bool,
    pub payment_intent: Option<String>,
    pub period_end: i64,
    pub period_start: i64,
    pub receipt_number: Option<String>,
    pub starting_balance: i64,
    pub statement_descriptor: Option<String>,
    pub status: InvoiceStatus,
    pub subscription: Option<String>,
    pub subscription_proration_date: Option<i64>,
    pub subtotal: i64,
    pub tax: Option<i64>,
    pub tax_percent: Option<f64>,
    pub total: i64,
    pub webhooks_delivered_at: Option<i64>,
}

#[derive(Deserialize, Debug)]
pub enum InvoiceBilling {
    #[serde(rename="charge_automatically")]
    ChargeAutomatcally,
    #[serde(rename="send_invoice")]
    SendInvoice
}

#[derive(Deserialize, Debug)]
pub enum InvoiceBillingReason {
    #[serde(rename="subscription_cycle")]
    SubscriptionCycle,
    #[serde(rename="subscription_create")]
    SubscriptionCreate,
    #[serde(rename="subscription_update")]
    SubscriptionUpdate,
    #[serde(rename="subscription")]
    Subscription,
    #[serde(rename="manual")]
    Manual,
    #[serde(rename="upcoming")]
    Upcoming
}


#[derive(Deserialize, Debug)]
pub enum InvoiceStatus {
    #[serde(rename="draft")]
    Draft,
    #[serde(rename="open")]
    Open,
    #[serde(rename="paid")]
    Paid,
    #[serde(rename="uncollectible")]
    Uncollectible,
    #[serde(rename="void")]
    Void
}

#[derive(Deserialize, Debug)]
pub struct InvoiceLine {
    pub id: String,
    pub object: Object,
    pub amount: i64,
    pub currency: Currency,
    pub description: Option<String>,
    pub discountable: bool,
    pub invoice_item: Option<String>,
    pub livemode: bool,
    pub metadata: HashMap<String, String>,
    pub period: Period,
    pub plan: Option<Plans>,
    pub proration: bool,
    pub quantity: Option<i64>,
    pub subscription: Option<String>,
    pub subscription_item: Option<String>,
    #[serde(rename="type")]
    pub invoiceline_type: InvoiceLineType
}

#[derive(Deserialize, Debug)]
pub enum InvoiceLineType {
    #[serde(rename="invoiceitem")]
    InvoiceItem,
    #[serde(rename="subscription")]
    Subscription
}

#[derive(Default, Serialize, Debug)]
pub struct InvoiceParam<'a> {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub customer: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub application_fee: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub metadata: Option<HashMap<&'a str, &'a str>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub statement_descriptor: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub subscription: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub tax_percent: Option<&'a str>,
}

#[derive(Default, Serialize, Debug)]
pub struct InvoiceLineParam<'a> {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub invoice: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub coupon: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub customer: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ending_before: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub limit: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub starting_after: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub subscription: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub subscription_items: Option<SubscriptionItems>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub subscription_prorate: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub subscription_proration_date: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub subscription_tax_percent: Option<f32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub subscription_trial_end: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub subscription_trial_from_plan: Option<&'a str>
}

#[derive(Default, Serialize, Debug)]
pub struct InvoiceListParams<'a> {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub billing: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub customer: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub date: Option<RangeQuery>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub due_date: Option<RangeQuery>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ending_before: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub limit: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub starting_after: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub subscription: Option<&'a str>,
}

impl StripeService for Invoice {}
impl StripeService for InvoiceLine {}
impl<'a> StripeService for InvoiceParam<'a> {}
impl<'a> StripeService for InvoiceListParams<'a> {}
impl<'a> StripeService for InvoiceLineParam<'a> {}


impl Invoice {
    
    pub fn create<B: serde::Serialize + StripeService>(client: &Client, param: B) -> Result<Self> {
        client.post(UrlPath::Invoices, &StripePath::default(), param)
    }

    pub fn retrieve(client: &Client, invoice: &str) -> Result<Self> {
        client.get(UrlPath::Invoices, &StripePath::default().param(invoice), Self::object())
    }

    pub fn retrieve_upcoming<B: serde::Serialize + StripeService>(client: &Client, param: B) -> Result<Self> {
        client.get(UrlPath::Invoices, &StripePath::default().param("upcoming"), param)
    }

    pub fn update<B: serde::Serialize + StripeService>(client: &Client, invoice_id: &str, param: B) -> Result<Self> {
        client.post(UrlPath::Invoices, &StripePath::default().param(invoice_id), param)
    }

    pub fn delete(client: &Client, invoice_id: &str) -> Result<Self> {
        client.delete(UrlPath::Invoices, &StripePath::default().param(invoice_id), Self::object())
    }

    pub fn finalize<B: serde::Serialize + StripeService>(client: &Client, invoice_id: &str, param: B) -> Result<Self> {
        client.post(UrlPath::Invoices, &StripePath::default().param(invoice_id).param("finalize"), param)
    }

    pub fn pay<B: serde::Serialize + StripeService>(client: &Client, invoice_id: &str, source: B) -> Result<Self> {
        client.post(UrlPath::Invoices, &StripePath::default().param(invoice_id).param("pay"), source)
    }

    pub fn send(client: &Client, invoice_id: &str) -> Result<Self> {
        client.post(UrlPath::Invoices, &StripePath::default().param(invoice_id).param("send"), Self::object())
    }

    pub fn void(client: &Client, invoice_id: &str) -> Result<Self> {
        client.post(UrlPath::Invoices, &StripePath::default().param(invoice_id).param("void"), Self::object())
    }

    pub fn uncollectible(client: &Client, invoice_id: &str) -> Result<Self> {
        client.post(UrlPath::Invoices, &StripePath::default().param(invoice_id).param("mark_uncollectible"), Self::object())
    }

    pub fn list<B: serde::Serialize + StripeService>(client: &Client, param: B) -> Result<List<Self>> {
        client.get(UrlPath::Invoices, &StripePath::default(), param)
    }

}

impl InvoiceLine {

    pub fn retrieve_upcoming_lines<B: serde::Serialize + StripeService>(client: &Client, param: B) -> Result<List<Self>> {
        client.get(UrlPath::Invoices, &StripePath::default().param("upcoming").param("lines"), param)
    }

    pub fn retrieve_lines<B: serde::Serialize + StripeService>(client: &Client, invoice: &str, param: B) -> Result<List<Self>> {
        client.get(UrlPath::Invoices, &StripePath::default().param(invoice).param("lines"), param)
    }
}