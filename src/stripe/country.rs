use serde;
use crate::stripe::{StripePath, UrlPath, Currency, Object};
use crate::util::List;
use crate::{Client, Result};
use std::collections::HashMap;
use crate::StripeService;



#[derive(Debug, PartialEq, Deserialize)]
pub struct CountrySpecs {
    pub id: String,
    pub object: Object,
    pub default_currency: Currency,
    pub supported_bank_account_currencies: HashMap<Currency, Vec<String>>,
    pub supported_payment_currencies: Vec<Currency>,
    pub supported_payment_methods: Vec<PaymentMethods>,
    pub verification_fields: Option<Verification>,

}

#[derive(Debug, PartialEq, Deserialize)]
pub struct Verification {
    pub individual: Option<EntityVerification>,
    pub company: Option<EntityVerification>
}

#[derive(Debug, PartialEq, Deserialize)]
pub struct EntityVerification {
    pub minimum: Option<Vec<MinimumVerification>>,
    pub additional: Option<Vec<AdditionalVerification>>
}


#[derive(Debug, PartialEq, Deserialize)]
pub enum MinimumVerification {
    #[serde(rename = "external_account")]
    EnternalAccount,
    #[serde(rename = "legal_entity.address.city")]
    LegalEntityAddressCity,
    #[serde(rename = "legal_entity.address.line1")]
    LegalEntityAddressLine1,
    #[serde(rename = "legal_entity.address.postal_code")]
    LegalEntityAddressPostalCode,
    #[serde(rename = "legal_entity.address.state")]
    LegalEntityAddressState,
    #[serde(rename = "legal_entity.business_name")]
    LegalEntityBusinessName,
    #[serde(rename = "legal_entity.business_tax_id")]
    LegalEntityTaxID,
    #[serde(rename = "legal_entity.dob.day")]
    LegalEntityDoBDay,
    #[serde(rename = "legal_entity.dob.month")]
    LegalEntityDoBMonth,
    #[serde(rename = "legal_entity.dob.year")]
    LegalEntityDoBYear,
    #[serde(rename = "legal_entity.first_name")]
    LegalEntityFirstName,
    #[serde(rename = "legal_entity.last_name")]
    LegalEntityLastName,
    #[serde(rename = "legal_entity.ssn_last_4")]
    LegalEntitySSNLast4,
    #[serde(rename = "legal_entity.type")]
    LegalEntityType,
    #[serde(rename = "tos_acceptance.date")]
    TOSAcceptanceDate,
    #[serde(rename = "tos_acceptance.ip")]
    TOSAcceptanceIP
}

#[derive(Debug, PartialEq, Deserialize)]
pub enum AdditionalVerification {
    #[serde(rename = "legal_entity.personal_id_number")]
    LegalEntityPersonalIDNumber,
    #[serde(rename = "legal_entity.verification.document")]
    LegalEntityVerificationDocument
}

#[derive(Debug, PartialEq, Eq, Deserialize)]
pub enum PaymentMethods {
    #[serde(rename = "alipay")]
    Alipay,
    #[serde(rename = "card")]
    Card,
    #[serde(rename = "stripe")]
    Stripe
}

#[derive(Default, Serialize, Debug)]
pub struct CountrySpecListParams<'a> {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ending_before: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub limit: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub starting_after: Option<&'a str>,
}

impl StripeService for CountrySpecs {}
impl<'a> StripeService for CountrySpecListParams<'a> {}

impl CountrySpecs {

    pub fn retrieve(client: &Client, country: &str) -> Result<Self> {
        client.get(UrlPath::CountrySpecs, &StripePath::default().param(country), Self::object())
    }

    pub fn list<B: serde::Serialize + StripeService>(client: &Client, param: B) -> Result<List<Self>> {
        client.get(UrlPath::CountrySpecs, &StripePath::default(), param)
    }
}