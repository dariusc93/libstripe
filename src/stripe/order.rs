use crate::stripe::{StripePath, UrlPath, ShippingDetails, Currency, Object};
use crate::util::List;
use crate::{Client, Result};
use serde;
use std::collections::HashMap;
use crate::StripeService;



#[derive(Deserialize, Debug)]
pub struct Order {
    pub id: String,
    pub object: Object,
    pub amount: i64,
    pub amount_returned: Option<i64>,
    pub application: Option<String>,
    pub application_fee: Option<i64>,
    pub charge: Option<OrderCharge>,
    pub created: i64,
    pub currency: Currency,
    pub customer: String,
    pub email: String,
    pub items: Vec<OrderItem>,
    pub livemode: bool,
    pub metadata: HashMap<String, String>,
    pub returns: List<OrderReturn>,
    pub selected_shipping_method: Option<String>,
    pub shipping: ShippingDetails,
    pub shipping_methods: Vec<ShippingMethods>,
    pub status: OrderStatus,
    pub status_transitions: OrderTransitions,
    pub updated: i64
}

#[derive(Deserialize, Debug)]
pub enum OrderCharge {
    #[serde(rename = "paid")]
    Paid,
    #[serde(rename = "fulfilled")]
    Fulfilled,
    #[serde(rename = "refunded")]
    Refunded
}

#[derive(Deserialize, Debug)]
pub struct DeliveryEstimate {
    #[serde(rename = "type")]
    pub delivery_type: DeliveryType,
    pub latest: Option<String>,
    pub earliest: Option<String>,
    pub date: Option<String>,
}

#[derive(Deserialize, Debug)]
pub enum DeliveryType {
    #[serde(rename = "range")]
    Range,
    #[serde(rename = "exact")]
    Exact
}

#[derive(Deserialize, Debug)]
pub struct ShippingMethods {
    pub id: String,
    pub amount: i64,
    pub currency: Currency,
    pub delivery_estimate: Option<DeliveryEstimate>,
    pub description: String
}

#[derive(Deserialize, Debug)]
pub struct OrderTransitions {
    pub canceled: i64,
    pub fulfiled: Option<i64>,
    pub paid: i64,
    pub returned: Option<i64>,
}

#[derive(Deserialize, Debug)]
pub struct OrderReturn {
    pub id: String,
    pub object: Object,
    pub amount: i64,
    pub created: i64,
    pub currency: Currency,
    pub items: Vec<OrderItem>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct OrderItem {
    #[serde(skip_serializing)]
    pub object: Object,
    pub amount: i64,
    pub currency: Currency,
    pub description: Option<String>,
    pub parent: String,
    pub quantity: Option<i64>,
    #[serde(rename = "type")]
    pub item_type: ItemType,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub enum ItemType {
    #[serde(rename = "sku")]
    Sku,
    #[serde(rename = "tax")]
    Tax,
    #[serde(rename = "shipping")]
    Shipping,
    #[serde(rename = "discount")]
    Discount,
    #[serde(skip_serializing)]
    Unknown
}

impl Default for ItemType {
    fn default() -> ItemType { ItemType::Unknown }
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub enum OrderStatus {
    #[serde(rename="created")]
    Created,
    #[serde(rename="paid")]
    Paid,
    #[serde(rename="canceled")]
    Canceled,
    #[serde(rename="fulfilled")]
    Fulfilled,
    #[serde(rename="returned")]
    Returned
}

#[derive(Default, Serialize, Debug)]
pub struct OrderParam<'a> {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub currency: Option<Currency>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub coupon: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub customer: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub email: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub items: Option<OrderItem>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub metadata: Option<HashMap<&'a str, &'a str>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub shipping: Option<ShippingDetails>,
}

impl StripeService for Order {}
impl<'a> StripeService for OrderParam<'a> {}

impl Order {
    
    pub fn create<B: serde::Serialize + StripeService>(client: &Client, param: B) -> Result<Self> {
        client.post(UrlPath::Order, &StripePath::default(), param)
    }

    pub fn retrieve(client: &Client, id: &str) -> Result<Self> {
        client.get(UrlPath::Order, &StripePath::default().param(id), Self::object())
    }

    pub fn update<B: serde::Serialize + StripeService>(client: &Client, id: &str, param: B) -> Result<Self> {
        client.post(UrlPath::Order, &StripePath::default().param(id), param)
    }

    pub fn pay<B: serde::Serialize + StripeService>(client: &Client, id: &str, param: B) -> Result<Self> {
        client.post(UrlPath::Order, &StripePath::default().param(id).param("pay"), param)
    }

    pub fn list<B: serde::Serialize + StripeService>(client: &Client, param: B) -> Result<List<Self>> {
        client.get(UrlPath::Order, &StripePath::default(), param)
    }

    pub fn return_item<B: serde::Serialize + StripeService>(client: &Client, id: &str, param: B) -> Result<Self> {
        client.post(UrlPath::Order, &StripePath::default().param(id).param("returns"), param)
    }
}