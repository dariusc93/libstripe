use crate::{Client, Result};
use crate::stripe::{StripePath, UrlPath, Currency, Address, BankAccount, Card, CardParam, BankAccountParam, Object};
use serde;
use std::collections::HashMap;
use crate::StripeService;



#[derive(Deserialize, Debug)]
pub struct Source {
    pub id: String,
    pub object: Object,
    pub ach_credit_transfer: Option<AchCreditTransfer>,
    pub amount: i32,
    pub client_secret: String,
    pub code_verification: CodeVerification,
    pub created: i64,
    pub currency: Currency,
    pub flow: SourceFlow,
    pub livemode: bool,
    pub metadata: HashMap<String, String>,
    pub owner: SourceOwner,
    pub receiver: Option<SourceReceiver>,
    pub statement_descriptor: Option<String>,
    pub status: SourceStatus,
    pub redirect: Option<SourceRedirect>,
    #[serde(rename="type")]
    pub source_type: SourceType,
    pub usage: SourceUsage,
}

#[derive(Deserialize, Debug)]
pub struct AchCreditTransfer {
    pub account_number: String,
    pub routing_number: String,
    pub fingerprint: String,
    pub bank_name: String,
    pub swift_code: String,
}

#[derive(Deserialize, Debug)]
pub struct CodeVerification {
    pub attempts_remaining: i64,
    pub status: VerificationStatus
}

#[derive(Deserialize, Debug)]
pub enum VerificationStatus {
    #[serde(rename="pending")]
    Pending,
    #[serde(rename="succeeded")]
    Succeeded,
    #[serde(rename="failed")]
    Failed
}

#[derive(Deserialize, Debug)]
pub struct SourceRedirect {
    pub return_url: String,
    pub status: SourceRedirectStatus,
    pub url: String
}

#[derive(Deserialize, Debug)]
pub enum SourceRedirectStatus {
    #[serde(rename="pending")]
    Pending,
    #[serde(rename="succeeded")]
    Succeeded,
    #[serde(rename="not_required")]
    NotRequired,
    #[serde(rename="failed")]
    Failed
}

#[derive(Serialize, Deserialize, Debug)]
pub struct SourceOwner {
    pub address: Option<Address>,
    pub email: Option<String>,
    pub name: Option<String>,
    pub phone: Option<String>,
    pub verified_address: Option<Address>,
    pub verified_email: Option<String>,
    pub verified_name: Option<String>,
    pub verified_phone: Option<String>,
}

#[derive(Deserialize, Debug)]
pub struct SourceReceiver {
    pub address: String,
    pub amount_charged: i32,
    pub amount_received: i32,
    pub amount_returned: i32,
    pub refund_attributes_method: String,
    pub refund_attributes_status: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub enum SourceFlow {
    #[serde(rename="redirect")]
    Redirect,
    #[serde(rename="receiver")]
    Receiver,
    #[serde(rename="code_verification")]
    CodeVerification,
    #[serde(rename="none")]
    None
}

#[derive(Serialize, Deserialize, Debug)]
pub enum SourceUsage {
    #[serde(rename="reusable")]
    Reusable,
    #[serde(rename="single_use")]
    SingleUse
}

#[derive(Deserialize, Debug)]
pub enum SourceStatus {
    #[serde(rename="canceled")]
    Canceled,
    #[serde(rename="chargeable")]
    Chargeable,
    #[serde(rename="consumed")]
    Consumed,
    #[serde(rename="failed")]
    Failed,
    #[serde(rename="pending")]
    Pending
}

#[derive(Serialize, Deserialize, Debug)]
pub enum SourceType {
    #[serde(rename="card")]
    Card,
    #[serde(rename="three_d_secure")]
    ThreeDSecure,
    #[serde(rename="giropay")]
    Giropay,
    #[serde(rename="sepa_debit")]
    SepaDebit,
    #[serde(rename="ideal")]
    Ideal,
    #[serde(rename="sofort")]
    Sofort,
    #[serde(rename="bancontact")]
    Bancontact,
    #[serde(rename="alipay")]
    Alipay,
}

#[derive(Debug, Deserialize)]
#[serde(untagged)]
pub enum PaymentSource {
    BankAccount(BankAccount),
    Card(Card),
}

#[derive(Serialize, Debug)]
#[serde(untagged)]
pub enum PaymentSourceParam<'a> {
    BankAccount(BankAccountParam<'a>),
    Card(CardParam<'a>),
    Token(&'a str)
}

#[derive(Default, Serialize, Debug)]
pub struct PaymentParam<'a> {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub source: Option<PaymentSourceParam<'a>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub metadata: Option<HashMap<String, String>>
}

#[derive(Default, Serialize, Debug)]
pub struct SourceParam<'a> {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "type")]
    pub source_type: Option<SourceType>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub amount: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub currency: Option<Currency>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub flow: Option<SourceFlow>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub metadata: Option<HashMap<&'a str, &'a str>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub owner: Option<SourceOwner>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub redirect: Option<SourceRedirectParam<'a>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub statement_descriptor: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub token: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub usage: Option<SourceUsage>,
}

#[derive(Serialize, Debug)]
pub struct SourceRedirectParam<'a> {
    pub return_url: &'a str
}

impl StripeService for Source {}
impl<'a> StripeService for SourceParam<'a> {}
impl<'a> StripeService for SourceRedirectParam<'a> {}
impl<'a> StripeService for PaymentParam<'a> {}


impl Source {

    pub fn create<B: serde::Serialize + StripeService>(client: &Client, param: B) -> Result<Self> {
        client.post(UrlPath::Sources, &StripePath::default(), param)
    }

    pub fn retrieve<B: serde::Serialize + StripeService>(client: &Client, id: &str, param: B) -> Result<Self> {
        client.get(UrlPath::Sources, &StripePath::default().param(id), param)
    }

    pub fn update<B: serde::Serialize + StripeService>(client: &Client, id: &str, param: B) -> Result<Self> {
        client.post(UrlPath::Sources, &StripePath::default().param(id), param)
    }
}