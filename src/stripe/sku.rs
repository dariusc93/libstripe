use crate::util::{Deleted, List};
use crate::{Client, Result};
use crate::stripe::{StripePath, UrlPath, Currency, Object};
use serde;
use std::collections::HashMap;
use crate::StripeService;



#[derive(Debug, Deserialize)]
pub struct Sku {
    pub id: String,
    pub object: Object,
    pub active: bool,
    pub attributes: Option<HashMap<String, String>>,
    pub created: i64,
    pub currency: Currency,
    pub image: Option<String>,
    pub inventory: Inventory,
    pub livemode: bool,
    pub metadata: HashMap<String, String>,
    pub package_dimensions: Option<PackageDimensions>,
    pub price: i64,
    pub product: String,
    pub updated: i64
}

#[derive(Default, Debug, Deserialize, Serialize)]
pub struct PackageDimensions {
    pub height: f64,
    pub length: f64,
    pub weight: f64,
    pub width: f64,
}

#[derive(Default, Debug, Deserialize, Serialize)]
pub struct Inventory {
    pub quantity: Option<i32>,
    #[serde(rename="type")]
    pub inventory_type: Option<InventoryType>,
    pub value: Option<InventoryValue>,
}

#[derive(Debug, Deserialize, Serialize)]
pub enum InventoryValue {
    #[serde(rename="in_stock")]
    InStock,
    #[serde(rename="limited")]
    Limited,
    #[serde(rename="out_of_stock")]
    OutOfStock,
}

#[derive(Debug, Deserialize, Serialize)]
pub enum InventoryType {
    #[serde(rename="finite")]
    Finite,
    #[serde(rename="bucket")]
    Bucket,
    #[serde(rename="infinite")]
    Infinite
}

#[derive(Default, Debug, Serialize)]
pub struct SkuParam<'a> {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub currency: Option<Currency>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub inventory: Option<Inventory>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub price: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub product: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub active: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub attributes: Option<HashMap<&'a str, &'a str>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub image: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub metadata: Option<HashMap<&'a str, &'a str>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub package_dimensions: Option<PackageDimensions>
}

impl StripeService for Sku {}

impl<'a> StripeService for SkuParam<'a> {}


impl Sku {

    pub fn create<B: serde::Serialize + StripeService>(client: &Client, param: B) -> Result<Self> {
        client.post(UrlPath::Sku, &StripePath::default(), param)
    }

    pub fn retrieve(client: &Client, id: &str) -> Result<Self> {
        client.get(UrlPath::Sku, &StripePath::default().param(id), Self::object())
    }

    pub fn update<B: serde::Serialize + StripeService>(client: &Client, id: &str, param: B) -> Result<Self> {
        client.post(UrlPath::Sku, &StripePath::default().param(id), param)
    }

    pub fn list<B: serde::Serialize + StripeService>(client: &Client, param: B) -> Result<List<Self>> {
        client.get(UrlPath::Sku, &StripePath::default(), param)
    }

    pub fn delete(client: &Client, id: &str) -> Result<Deleted> {
        client.delete(UrlPath::Sku, &StripePath::default().param(id), Self::object())
    }

}

