use crate::stripe::{Object, Address, StripePath, UrlPath};
use crate::{Client, Result, StripeService};
use crate::util::List;
use std::collections::HashMap;


use serde;

#[derive(Serialize, Deserialize, Debug)]
pub struct CardHolders {
    pub id: String,
    pub object: Object,
    pub billing: Billing,
    pub created: i64,
    pub email: String,
    pub livemode: bool,
    pub metadata: HashMap<String, String>,
    pub name: String,
    pub phone_number: String,
    pub status: CardHolderStatus,
    pub cardholder_type: CardHolderType

}

#[derive(Serialize, Deserialize, Debug)]
pub struct Billing {
    pub address: Address,
    pub name: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub enum CardHolderStatus {
    Active,
    Inactive,
    Pending
}

#[derive(Serialize, Deserialize, Debug)]
pub enum CardHolderType {
    Individual,
    BusinessEntity //TODO: Rename
}

#[derive(Serialize, Deserialize, Debug)]
pub struct CardHolderParam<'a> {
    pub billing: Option<Billing>,
    pub name: Option<&'a str>,
    pub cardholder_type: Option<CardHolderType>,
    pub email: Option<&'a str>,
    pub metadata: Option<HashMap<String, String>>,
    pub phone_number: Option<&'a str>,
    pub status: Option<CardHolderStatus>
}

impl StripeService for CardHolders {}
impl<'a> StripeService for CardHolderParam<'a> {}

impl CardHolders {
    pub fn create<B: serde::Serialize + StripeService>(client: &Client, param: B) -> Result<Self> {
        client.post(UrlPath::CardHolders, &StripePath::default(), param)
    }

    pub fn retrieve(client: &Client, id: &str) -> Result<Self> {
        client.get(UrlPath::CardHolders, &StripePath::default().param(id), Self::object())
    }

    pub fn update<B: serde::Serialize + StripeService>(client: &Client, id: &str, param: B) -> Result<Self> {
        client.post(UrlPath::CardHolders, &StripePath::default().param(id), param)
    }

    pub fn list<B: serde::Serialize + StripeService>(client: &Client, param: B) -> Result<List<Self>> {
        client.get(UrlPath::CardHolders, &StripePath::default(), param)
    }

}