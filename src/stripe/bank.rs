use crate::{Client, Result, StripeService};
use crate::stripe::{StripePath, UrlPath, Currency, Object};
use crate::util::{List, Deleted};
use serde;
use std::collections::HashMap;



#[derive(Deserialize, Debug)]
pub struct BankAccount {
    pub id: String,
    pub object: Object,
    pub account: Option<String>,
    pub account_holder_name: String,
    pub account_holder_type: AccountHolderType,
    pub bank_name: String,
    pub country: String,
    pub customer: Option<String>,
    pub currency: Currency,
    pub default_for_currency: Option<bool>,
    pub fingerprint: String, 
    pub last4: String,
    pub metadata: HashMap<String, String>,
    pub routing_number: String,
    pub status: BankStatus,
}

#[derive(Default, Serialize, Debug)]
pub struct BankAccountParam<'a> {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub object: Option<Object>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub account_number: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub country: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub currency: Option<Currency>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub account_holder_name: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub account_holder_type: Option<AccountHolderType>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub routing_number: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub metadata: Option<HashMap<String, String>>,
}

//NOTE: Workaround to add an object name while leaving the rest "default"
impl<'a> BankAccountParam<'a> {
    pub fn default() -> Self {
        BankAccountParam {
            object: Some(Object::BankAccount),
            ..Default::default()
        }
    }
}

#[derive(Default, Serialize, Debug)]
pub struct BankAccountVerifyParam {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub amounts: Option<(f32, f32)>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub verification_method: Option<String> //??
}

#[derive(Serialize, Debug)]
pub struct BankListParams<'a> {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ending_before: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub limit: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub starting_after: Option<&'a str>,
}


#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub enum AccountHolderType {
    #[serde(rename = "individual")]
    Individual,
    #[serde(rename = "company")]
    Company
}

#[derive(Deserialize, PartialEq, Debug)]
pub enum BankStatus {
    #[serde(rename = "new")]
    New,
    #[serde(rename = "validated")]
    Validated,
    #[serde(rename = "verified")]
    Verified,
    #[serde(rename = "verification_failed")]
    VerificationFailed,
    #[serde(rename = "errored")]
    Errored
}

impl StripeService for BankAccount {}
impl<'a> StripeService for BankAccountParam<'a> {}
impl<'a> StripeService for BankListParams<'a> {}

impl BankAccount {
    
    pub fn create<B: serde::Serialize + StripeService>(client: &Client, customer_id: &str, param: B) -> Result<Self> {
        client.post(UrlPath::Customers, &StripePath::default().param(customer_id).param("sources"), param)
    }

    pub fn retrieve(client: &Client, customer_id: &str, id: &str) -> Result<Self> {
        client.get(UrlPath::Customers, &StripePath::default().param(customer_id).param("sources").param(id), Self::object())
    }

    pub fn update<B: serde::Serialize + StripeService>(client: &Client, customer_id: &str, id: &str, param: B) -> Result<Self> {
        client.post(UrlPath::Customers, &StripePath::default().param(customer_id).param("sources").param(id), param)
    }

    pub fn verify<B: serde::Serialize + StripeService>(client: &Client, customer_id: &str, id: &str, param: B) -> Result<Self> {
        client.post(UrlPath::Customers, &StripePath::default().param(customer_id).param("sources").param(id).param("verify"), param)
    }

    pub fn delete(client: &Client, customer_id: &str, id: &str) -> Result<Deleted> {
        client.delete(UrlPath::Customers, &StripePath::default().param(customer_id).param("sources").param(id), Self::object())
    }

    pub fn list<B: serde::Serialize + StripeService>(client: &Client, customer_id: &str, param: B) -> Result<List<Self>> {
        client.get(UrlPath::Customers, &StripePath::default().param(customer_id).param("sources"), param)
    }
    
}