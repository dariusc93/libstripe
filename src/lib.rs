
#[macro_use] extern crate serde_derive;

mod client;
mod util;
mod error;
mod stripe;

pub use crate::client::Client;
pub use crate::error::*;
pub use crate::stripe::*;

//use error::Error;
use serde_json::{Value, map::Map};

pub type Result<T> = ::std::result::Result<T, Error>;

pub trait StripeService {

    fn uri(&self, path: UrlPath, param: &stripe::StripePath) -> String {
        format!("https://api.stripe.com/v1{}{}", path, param)
    }

    fn object() -> Value {
        Value::Object(Map::new())
    }

    fn empty(&self) -> Value {
        Self::object()
    }

}

impl StripeService for Value {}

//#[derive(Serialize, Deserialize, Debug)]
//pub struct ObjectEmpty(Value);
//
//impl StripeService for ObjectEmpty {}