
use serde_json;
use serde_qs;
use http;
use crate::util::to_snakecase;
use std::error;
use std::fmt;
use std::io;
use reqwest;

#[derive(Debug)]
pub enum Error {
    Stripe(StripeErrorObject),
    Http(reqwest::Error),
    Json(serde_json::error::Error),
    Uri(http::uri::InvalidUri),
    Io(io::Error),
    Conversion(Box<error::Error + Send>),
    HeaderError(reqwest::header::InvalidHeaderValue),
    Unknown
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Error::Stripe(ref err) => write!(f, ": {:?}", err),
            Error::Http(ref err) => write!(f, ": {}", err),
            Error::Json(ref err) => write!(f, ": {}", err),
            Error::Uri(ref err) => write!(f, ": {}", err),
            Error::Io(ref err) => write!(f, ": {}", err),
            Error::Conversion(ref err) => write!(f, ": {}", err),
            Error::HeaderError(ref err) => write!(f, ": {}", err),
            Error::Unknown => write!(f, ": unknown error"),
        }
    }
}


impl From<StripeErrorObject> for Error {
    fn from(err: StripeErrorObject) -> Error {
        Error::Stripe(err)
    }
}

impl From<reqwest::Error> for Error {
    fn from(err: reqwest::Error) -> Error {
        Error::Http(err)
    }
}

impl From<reqwest::header::InvalidHeaderValue> for Error {
    fn from(err: reqwest::header::InvalidHeaderValue) -> Error {
        Error::HeaderError(err)
    }
}

impl From<serde_json::Error> for Error {
    fn from(err: serde_json::Error) -> Error {
        Error::Json(err)
    }
}

impl From<http::uri::InvalidUri> for Error {
    fn from(err: http::uri::InvalidUri) -> Error {
        Error::Uri(err)
    }
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Error {
        Error::Io(err)
    }
}

impl From<serde_qs::Error> for Error {
    fn from(err: serde_qs::Error) -> Error {
        Error::Conversion(Box::new(err))
    }
}


#[derive(Debug, PartialEq, Deserialize, Serialize)]
pub enum ErrorType {
    #[serde(skip_deserializing)]
    Unknown,
    #[serde(rename = "api_error")]
    Api,
    #[serde(rename = "api_connection_error")]
    Connection,
    #[serde(rename = "authentication_error")]
    Authentication,
    #[serde(rename = "card_error")]
    Card,
    #[serde(rename = "invalid_request_error")]
    InvalidRequest,
    #[serde(rename = "rate_limit_error")]
    RateLimit,
    #[serde(rename = "validation_error")]
    Validation,
}

impl Default for ErrorType {
    fn default() -> Self {
        ErrorType::Unknown
    }
}

impl fmt::Display for ErrorType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", to_snakecase(&format!("{:?}Error", self)))
    }
}

#[derive(Debug, PartialEq, Deserialize, Serialize)]
pub enum ErrorCode {
    #[serde(rename = "invalid_number")]
    InvalidNumber,
    #[serde(rename = "invalid_expiry_month")]
    InvalidExpiryMonth,
    #[serde(rename = "invalid_expiry_year")]
    InvalidExpiryYear,
    #[serde(rename = "invalid_cvc")]
    InvalidCvc,
    #[serde(rename = "invalid_swipe_data")]
    InvalidSwipeData,
    #[serde(rename = "incorrect_number")]
    IncorrectNumber,
    #[serde(rename = "expired_card")]
    ExpiredCard,
    #[serde(rename = "incorrect_cvc")]
    IncorrectCvc,
    #[serde(rename = "incorrect_zip")]
    IncorrectZip,
    #[serde(rename = "card_declined")]
    CardDeclined,
    #[serde(rename = "missing")]
    Missing,
    #[serde(rename = "processing_error")]
    ProcessingError,
    #[serde(rename = "parameter_missing")]
    ParameterMissing,
    #[serde(rename = "account_already_exists")]
    AccountAlreadyExists,
    #[serde(rename = "account_country_invalid_address")]
    AccountCountryInvalidAddress,
    #[serde(rename = "account_invalid")]
    AccountInvalid,
    #[serde(rename = "account_number_invalid")]
    AccountNumberInvalid,
    #[serde(rename = "alipay_upgrade_required")]
    AlipayUpgradeRequired,
    #[serde(rename = "amount_too_large")]
    AmountTooLarge,
    #[serde(rename = "amount_too_small")]
    AmountTooSmall,
    #[serde(rename = "api_key_expired")]
    ApiKeyExpired,
    #[serde(rename = "balance_insufficient")]
    BalanceInsufficient,
    #[serde(rename = "bank_account_exists")]
    BankAccountExists,
    #[serde(rename = "bank_account_unusable")]
    BankAccountUnusable,
    #[serde(rename = "bank_account_unverified")]
    BankAccountUnverified,
    #[serde(rename = "charge_already_captured")]
    ChargeAlreadyCaptured,
    #[serde(rename = "charge_already_refunded")]
    ChargeAlreadyRefunded,
    #[serde(rename = "charge_expired_for_capture")]
    ChargeExpiredForCapture,
    #[serde(rename = "country_unsupported")]
    CountryUnsupported,
    #[serde(rename = "coupon_expired")]
    CouponExpired,
    #[serde(rename = "customer_max_subscriptions")]
    CustomerMaxSubscriptions,
    #[serde(rename = "email_invalid")]
    EmailInvalid,
    #[serde(rename = "instant_payouts_unsupported")]
    InstantPayoutsUnsupported,
    #[serde(rename = "invalid_card_type")]
    InvalidCardType,
    #[serde(rename = "invalid_charge_amount")]
    InvalidChargeAmount,
    #[serde(rename = "invalid_source_usage")]
    InvalidSourceUsage,
    #[serde(rename = "invoice_no_customer_line_items")]
    InvoiceNoCustomerLineItems,
    #[serde(rename = "invoice_no_subscription_line_items")]
    InvoiceNoSubscriptionLineItems,
    #[serde(rename = "invoice_not_editable")]
    InvoiceNotEditable,
    #[serde(rename = "invoice_upcoming_none")]
    InvoiceUpcomingNone,
    #[serde(rename = "livemode_mismatch")]
    LivemodeMismatch,
    #[serde(rename = "order_creation_failed")]
    OrderCreationFailed,
    #[serde(rename = "order_required_settings")]
    OrderRequiredSettings,
    #[serde(rename = "order_status_invalid")]
    OrderStatusInvalid,
    #[serde(rename = "order_upstream_timeout")]
    OrderUpstreamTimeout,
    #[serde(rename = "out_of_inventory")]
    OutOfInventory,
    #[serde(rename = "parameter_invalid_empty")]
    ParameterInvalidEmpty,
    #[serde(rename = "parameter_invalid_integer")]
    ParameterInvalidInteger,
    #[serde(rename = "parameter_invalid_string_blank")]
    ParameterInvalidStringBlank,
    #[serde(rename = "parameter_invalid_string_empty")]
    ParameterInvalidStringEmpty,
    #[serde(rename = "parameter_unknown")]
    ParameterUnknown,
    #[serde(rename = "payment_method_unactivated")]
    PaymentMethodUnactivated,
    #[serde(rename = "payout_not_allowed")]
    PayoutNotAllowed,
    #[serde(rename = "platform_api_key_expired")]
    PlatformApiKeyExpired,
    #[serde(rename = "postal_code_invalid")]
    PostalCodeInvalid,
    #[serde(rename = "product_inactive")]
    ProductInactive,
    #[serde(rename = "rate_limit")]
    RateLimit,
    #[serde(rename = "resource_already_exists")]
    ResourceAlreadyExists,
    #[serde(rename = "resource_missing")]
    ResourceMissing,
    #[serde(rename = "routing_number_invalid")]
    RoutingNumberInvalid,
    #[serde(rename = "secret_key_required")]
    SecretKeyRequired,
    #[serde(rename = "sepa_unsupported_account")]
    SepaUnsupportedAccount,
    #[serde(rename = "shipping_calculation_failed")]
    ShippingCalculationFailed,
    #[serde(rename = "sku_inactive")]
    SkuInactive,
    #[serde(rename = "state_unsupported")]
    StateUnsupported,
    #[serde(rename = "tax_id_invalid")]
    TaxIdInvalid,
    #[serde(rename = "taxes_calculation_failed")]
    TaxesCalculationFailed,
    #[serde(rename = "testmode_charges_only")]
    TestmodeChargesOnly,
    #[serde(rename = "tls_version_unsupported")]
    TlsVersionUnsupported,
    #[serde(rename = "token_already_used")]
    TokenAlreadyUsed,
    #[serde(rename = "token_in_use")]
    TokenInUse,
    #[serde(rename = "transfers_not_allowed")]
    TransfersNotAllowed,
    #[serde(rename = "upstream_order_creation_failed")]
    UpstreamOrderCreationFailed,
    #[serde(rename = "url_invalid")]
    UrlInvalid,
    #[serde(rename = "invalid_utf8_in_post_body")]
    InvalidUtf8InPostBody
}

impl fmt::Display for ErrorCode {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", to_snakecase(&format!("{:?}", self)))
    }
}

#[derive(Debug, Default, Deserialize)]
pub struct StripeErrorObject {
    pub error: StripeRequestObject,
}

#[derive(Debug, Default, Deserialize)]
pub struct StripeRequestObject {
    #[serde(rename = "type")]
    pub error_type: ErrorType,
    pub charge: Option<String>,
    #[serde(default)]
    pub message: Option<String>,
    pub code: Option<ErrorCode>,
    pub decline_code: Option<String>,
    pub param: Option<String>,
    pub doc_url: Option<String>
}

impl fmt::Display for StripeErrorObject {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.error.error_type)?;
        if let Some(ref message) = self.error.message {
            write!(f, ": {}", message)?;
        }
        Ok(())
    }
}

//impl error::Error for StripeErrorObject {
//    fn description(&self) -> &str {
//        self.error.message.as_ref().map(|s| s.as_str()).unwrap_or(
//            "request error",
//        )
//    }
//}