Switch from reqwest to hyper
Implement tokio and futures for async request
Implement an API around futures to support async and non-async request and task
Simply API to make it easier to expand with common calls.
Create a queue system to queue task and execute them in order or async (maybe just use Future::join)
Cleanup codebase