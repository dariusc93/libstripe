
use libstripe::*;
use std::env;

fn main() -> Result<()> {
    let secret_key = env::var("STRIPE_KEY").expect("Missing 'STRIPE_KEY'.");
    let client = Client::new(&secret_key);

    let mut customer_param = CustomerParam::default();
    customer_param.email = Some("example@example.com");
    customer_param.description = Some("Example account");
    let customer = Customer::create(&client, customer_param)?;

    let mut payment_param = PaymentParam::default();
    payment_param.source = Some(PaymentSourceParam::Card({
        let mut card_param = CardParam::default();
        card_param.name = Some("John Doe");
        card_param.number = Some("4242424242424242");
        card_param.exp_month = Some("01");
        card_param.exp_year = Some("2021");
        card_param.cvc = Some("000");
        card_param.address_line1 = Some("123 Anon St");
        card_param.address_city = Some("City");
        card_param.address_state = Some("");
        card_param.address_country = Some("");
        card_param.address_zip = Some("");
        card_param
    }));

    let card = Card::create(&client, &customer.id, payment_param)?;

    println!("{:?}", card);

    Ok(())
}