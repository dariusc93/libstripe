
use libstripe::*;
use std::env;

fn main() -> Result<()> {
    let secret_key = env::var("STRIPE_KEY").expect("Missing 'STRIPE_KEY'.");
    let client = Client::new(&secret_key);

    let mut param = CustomerParam::default();

    param.email = Some("example@example.com");
    param.description = Some("Example account");
    
    let customer = Customer::create(&client, param)?;

    println!("{:?}", customer);

    let deleted = Customer::delete(&client, &customer.id)?;

    println!("{:?}", deleted);

    Ok(())
}