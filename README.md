libstripe


**REPOSITORY HAS MOVED TO https://github.com/bundleofbytes/libstripe**

===========

![Status](https://img.shields.io/badge/status-active-brightgreen.svg?style=flat)
[![](http://meritbadge.herokuapp.com/libstripe)](https://crates.io/crates/libstripe)

Stripe library for rust.

Note: Everything is subject to change but everything should be stable to use.

## Example

```rust
extern crate libstripe;

use libstripe::*;

fn main() {
    let client = Client::new("sk_test_..............");

    let mut param = CustomerParam::default();

    param.email = Some("example@example.com");
    param.description = Some("Example account");

    let customer = match Customer::create(&client, param) {
        Ok(cust) => cust,
        Err(e) => panic!("{}", e)
    };

    println!("{:?}", customer);

}
```
